const expect = require("chai").expect
const router = require("../src/router")
const PORT = 4000
const HOST = "127.0.0.1"
const baseURL = `http://${HOST}:${PORT}`

describe("Test routes", () => {
    let server
    before("Start server", (done) => {
        server = router.listen(
            PORT,
            HOST,
            () => done()
        )
    })
    describe("Routes", () => {
        it("Can GET welcome message", async () => {
            const response = await fetch(baseURL)
            const response_msg = await response.text()
            expect(response_msg).to.equal("Welcome!")
        })
        it("Can GET sum of two numbers", async () => {
            const endpoint = baseURL + "/add"
            const getResponse = (a, b) => new Promise(async (resolve, reject) => {
                const query = `${endpoint}?a=${a}&b=${b}`
                const response = await(await fetch(query)).text()
                resolve(response)
            })
            expect(await getResponse(1,2)).to.equal("3")
        })
    })
})