const express = require('express')
const router = express()
const PORT = 4000

// router endpoint
router.get('/', (req, res) => {
    res.send("Welcome!")
})

// router endpoint to add two numbers together. Query parameters are "a" and "b".
router.get("/add", (req, res) => {
    try{
        const sum = parseFloat(req.query.a) + parseFloat(req.query.b)
        res.send(sum.toString())
    } catch (e) {
        res.sendStatus(500)
    }
})

// new router endpoint (for Software licensing and versioning task)
router.get('/new', (req, res) => {
    res.send("New feature!")
})

module.exports = router