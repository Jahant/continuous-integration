// initialize
console.log("Program starting")
const PORT = 4000
const HOST = "127.0.0.1"
const router = require('./router')
// operate
router.listen(
    PORT,
    HOST, () => console.log(`Listening to http://${HOST}:${PORT}`))
// cleanup
console.log("Program ending")
